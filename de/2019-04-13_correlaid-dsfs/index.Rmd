---
title: "Let's git our sh*t together!"
author: "Frie Preu"
date: "2019-04-20"
output:
  xaringan::moon_reader:
    css: [default, metropolis, metropolis-fonts]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---


## Who am I? 
- Frie Preu
- data scientist / IT consultant / software developer bei codecentric 
- bei CorrelAid: IT Infrastruktur, interne Projekte (R Packages, ...) 
- Add me:
    - LinkedIn: [Friedrike Preu](https://www.linkedin.com/in/friedrike-preu-a2bb46a7/)
    - Twitter: [@ameisen_strasse](https://twitter.com/ameisen_strasse)
    - GitHub: [friep](https://github.com/friep)
    - GitLab: [friep](https://gitlab.com/friep)
---


### Installation und Accounterstellung
- siehe `README` des Repositories
- [www.gitlab.com/friep/git-our-shit-together](www.gitlab.com/friep/git-our-shit-together)


---

## Partner up!

.pull-left[
  ### Mona Lovalace Octocat
  ![Mona](../../images/mona-lovelace.jpg)

-> [Ada Lovelace](https://en.wikipedia.org/wiki/Ada_Lovelace)

]
.pull-right[
  ### Grace Hopper Octocat
  ![Grace](../../images/gracehoppertocat.jpg)

-> [Grace Hopper](https://en.wikipedia.org/wiki/Grace_Hopper)

]

---

## Warum Git?

- Masterarbeit.docx
--

- Masterarbeit_v1.docx
--

- Masterarbeit_FINAL.docx
--

- Masterarbeit_FINAL_TimsKommentare.docx
--

- Masterarbeit_FINAL_FINAL.docx


---

class: center, middle 

![Help](https://media.giphy.com/media/phJ6eMRFYI6CQ/giphy.gif)

---

### Version Control to the Rescue!
- Beispiel: [https://github.com/friep/git-our-shit-together-test-repo/](https://github.com/friep/git-our-shit-together-test-repo/)
- RMarkdown Präsentation

---

class: center, middle, inverse

# Daten runterladen - Fork und Clone

---

## Fork und clone

### Repository
> A Git repository is a virtual storage of your project. It allows you to save versions of your code, which you can access when needed. ([Source](https://www.atlassian.com/git/tutorials/setting-up-a-repository))


---

## Fork und Clone

### Fork
> A fork is a copy of a repository. Forking a repository allows you to freely experiment with changes without affecting the original project. ([Source](https://help.github.com/articles/fork-a-repo/))

### Clone 
> Cloning a repository syncs it to your local machine. After you clone, you can add and edit files and then push and pull updates.  ([Source](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html))

---

## Hands On 1 - forke ein Repo
 
### Mona


1. [https://github.com/friep/git-our-shit-together-test-repo/](https://github.com/friep/git-our-shit-together-test-repo/): Fork (oben rechts)
2. `https://github.com/{USERNAME}/git-our-shit-together-test-repo` öffnet sich
3. unter Settings->Collaborators Grace hinzufügen

---
class: center, middle, inverse

# Detour: SSH vs HTTPS 

---

## SSH vs HTTPS

![Git clone](../../images/gitclone_https.png)


-> beeinflusst, wie GitHub dich *authentifiziert*

---

## Authentifizierung 

.pull-left[
  ### HTTPS
  - mit GitHub username + password
  - Gitkraken: funktioniert *einfach so* `r emo::ji('tada')`
  - Command line: evtl. muss häufiger username + password eingeben
]
.pull-right[
 ### SSH
 - mit einem *keypair* (`id_rsa.pub` und `id_rsa`)
 - public key, private key cryptography (siehe z.B. [Youtube](https://www.youtube.com/watch?v=AQDCe585Lnc))
 - Vorteil: nur einmal aufsetzen, muss kein Passwort merken
 - clone `ssh://...`
]

---

## Hands On 1.1: Gitkraken mit Github verbinden

1. Gitkraken Profil (rechts oben)
2. Preferences->Authentification->GitHub
3. connect to GitHub
4. Generate SSH key and add to GitHub

---

## Hands On 2 - clone ein Repo
### Mona & Grace

![Git clone](../../images/github/gitclone.png)



1. Gitkraken: Clone Repo -> Clone with URL
2. kopierten Link in URL Feld eingeben

---

## Real life 

- Open Source: fork + clone
- Job / mit Freund\*innen / CorrelAid: nur clone (wird als collaborator hinzugefügt)

---

class: center, middle, inverse


# Daten speichern - Add und Commit 

---
## Commit

### Commit 
> A commit is the Git equivalent of a "save".[...] Git committing is an operation that acts upon a collection of files and directories. ([Source](https://www.atlassian.com/git/tutorials/saving-changes))

--> Commit = Ein "Speicherpunkt" in Git. 

---
## Commit

- Commit hält **Veränderungen** gegenüber dem vorherigen Commit fest
    - Änderungen von Dateien
    - Neuerstellung von Dateien
    - Löschung von Dateien
    - Umbenennung von Dateien
- ein Commit kann mehrere Änderungen beinhalten!

---

### Adding und Staging Area 

![Staging Area](../../images/other/staging.png)

(Source: [https://git-scm.com/about/staging-area](https://git-scm.com/about/staging-area))

---
class: center, middle

## Commit history

![Git commits](../../images/gitkraken/gitkraken_commits.png)

---
class: center, middle

## Go back in time! 

![Time Machine](https://media.giphy.com/media/Vqvr9BGv1vhDi/giphy.gif)

---

## Hands On 3 - Go back in time 


1. `reset master to this commit`
2. spiele mit: `hard`, `mixed`, `soft`
3. `fast forward master to origin/master` (oberster Commit)

---


class: center, middle, inverse

# Detour: RMarkdown undf xaringan

---

## RMarkdown
- R *Flavor* vom Markdown (= markup language)
- sehr einfach...
  - Notizen 
  - Texte
  - Präsentationen
  - ... zu schreiben ...
- und sie zu gut ausschauenden Dokumenten zu *rendern*
- RMarkdown kann **R code** beinhalten...
- [https://rmarkdown.rstudio.com/lesson-1.html](https://rmarkdown.rstudio.com/lesson-1.html)


---

## xaringan
- eins von mehreren Packages für RMarkdown Präsentationen
- `install.packages("xaringan")` 
- `xaringan::inf_mr()` mit `index.Rmd` geöffnet

---


## Hands On 4 - einen Commit machen

### Grace + Mona

1.  Verändere etwas! (z.B. `README.md` oder `index.Rmd`)
2. **GIT ADD** von den "Unstaged Files" Dateien **GIT ADD**en, die man in Git "speichern" möchte. 
3. (halbwegs) aussagekräftige Commit Message schreiben
4. **GIT COMMIT** 


---
class: center, middle

## Git quizzed!

![Lokal](../../images/other/git_workflow_lokal_without_solution.png)

---
class: center, middle

## Git quizzed!


![Lokal](../../images/other/git_workflow_lokal_with_solution.png)

---

# Daten syncen - Push und Pull

---


## Git  Hosting

- GitHub: `r icon::fa("github")` 
- GitLab: `r icon::fa("gitlab")` 
- Keybase: `r icon::fa("keybase")` 

---

## Git local und git remote

... what? 

- **Lokal**: dein PC
- **Remote**: in der Cloud (GitHub, GitLab, ...)

![Gitkraken Lokal Remote](../../images/gitkraken/gitkraken_remote_lokal.png)

---


### Sync: Git Pull und Git Push

- Git Pull: neue Commits von GitHub / Gitlab downloaden
- Git Push: lokal erstellte Commits nach GitHub / Gitlab hochladen

---
class: center, middle

### Sync: Git Pull und Git Push

![Push Pull](../../images/gitkraken/push_pull.png)

---
class: center, middle

![Push the button](https://media.giphy.com/media/139lMwJ9ow7bKE/giphy.gif)

---


## Hands On 5 - Pull und Push

1. Grace: Push 
2. Mona: Pull 
3. Mona: Push 
4. Grace: Pull

---
class: center, middle

## Git quizzed!

![Push Pull](../../images/other/git_workflow_with_github_without_solution.png)

---
class: center, middle


## Git quizzed!

![Push Pull](../../images/other/git_workflow_with_github_with_solution.png)

---
class: center, middle, inverse

# When things go wrong...

---
class: center, middle

![git google](../../images/other/giterrors.png)

---
class: center, middle

![xccd](../../images/other/xkcd_comic.png)


---

## When things go wrong...

1. so lange nichts gepusht ist, alles (halbwegs) gut --> nicht pushen, wenn irgendwie alles seltsam ist
2. zur Not: Codestand sichern und neu clonen 

---

## Merge conflicts

> Merge conflicts occur when competing changes are made to the same line of a file, or when one person edits a file and another person deletes the same file. ([Source](https://help.github.com/en/articles/resolving-a-merge-conflict-using-the-command-line))

---

## Merge conflicts
1. du committest changes
--

2. du pullst, um die neusten Commits zu bekommen, bevor du pusht
--

3. Dein\*e Kolleg\*in hat etwas geändert, das mit deinen Changes clasht

--

4. git: 

---
class: middle, center

![up to you](https://media.giphy.com/media/xUOxfcCFf8z89a6KTC/giphy.gif)

---
## Merge Conflicts lösen

- müssen lokal und **manuell** gelöst werden --> entscheiden, welche Version man beibehält
- integriertes [Merge Conflict Tool in Gitkraken](https://blog.axosoft.com/merge-conflict-tool/)

  
---

## Hands on 6: Merge Conflicts

1. Mona oder Grace: editiert **auf GitHub** das Readme in einer Zeile
2. Mona & Grace: editiert bei sich lokal das Readme in der gleichen Zeile (etwas anderes)
3. Mona & Grace: eigene Änderung committen (nicht pushen!)
4. Mona & Grace: `pull` 
5. Mona & Grace: Merge Conflict lokal lösen und die eigene Version pushen.

---


## Sidenote: Git stash

-> Gitkraken macht häufig automatisch *stashes* um Veränderungen zu sichern 

> git stash temporarily shelves (or stashes) changes you've made to your working copy so you can work on something else, and then come back and re-apply them later on. Stashing is handy if you need to quickly switch context and work on something else, but you're mid-way through a code change and aren't quite ready to commit. ([Source](https://www.atlassian.com/git/tutorials/saving-changes/git-stash))

---
class: center, middle

## Sidnote: Git stash

![stash](https://media.giphy.com/media/l0HlxJfVUKhtR8Jna/giphy.gif)

--> put it away for now!

---

## Git stash um Merge Conflicts zu vermeiden


1. git stash
2. git pull
3. apply stash 
4. Merge Conflicts lösen
5. (delete Stash)



---
class: center, middle, inverse

# Mit GitHub arbeiten

---
class: center, middle, inverse

# Issues
---

## Issues


- Issues: Todos / Bugs / Ideen
- jeder Issue hat eine Nummer
- \#issueno in Commit message verknüpft Commit mit Issue

- Beispiel: [https://github.com/jandix/sealr/issues](https://github.com/jandix/sealr/issues) 

---

## Hands On 7: Issues


1. Mona: erstelle einen neuen Issue: "Add Grace's favorite GIF"
2. Grace: füge der Präsentation eine neue Folie hinzu mit deinem Lieblingsgif (giphy -> copy link) 
3. Grace: add + commit. Verlinke die Issue Nummer in der commit message (\#issueno)
4. Grace: push
5. Mona: Issue neu laden (STRG+R)

---

## Recap Issues


- sehr nützlich zur Projektorganisation
- Diskussion ist zentralisiert (aber: keine sensiblen Informationen in GitHub!)
- Tipp: Projekt mit Kanban Board (siehe "Projects" in GitHub) zum Prozesstracking (todo, in progress, done)

--> bitte verwenden!

---

## Real life
- Open Source: sehr verbreitet
- corporate: eher andere Tools zur Ticketverwaltung (Jira,...)

---
class: center, inverse, middle 

# Branches und Pull Requests

---
class: center, middle


![Gitkraken branching](../../images/gitkraken/screenshot_branching_gitkraken.png)

---
class: center, middle

![Come on](https://media.giphy.com/media/HfFccPJv7a9k4/giphy.gif)

---


## Branches

### Branch
> A branch represents an independent line of development. Branches serve as an abstraction for the edit/stage/commit process. You can think of them as a way to request a brand new working directory, staging area, and project history. ([Source](https://www.atlassian.com/git/tutorials/using-branches))


### Checkout

> The git checkout command lets you navigate between the branches created by git branch. Checking out a branch updates the files in the working directory to match the version stored in that branch, and it tells Git to record all new commits on that branch. Think of it as a way to select which line of development you’re working on. ([Source](https://www.atlassian.com/git/tutorials/using-branches/git-checkout))

---

## Warum Branches?

- Stabilität: Nur working code auf "master" branch 
  - CI/CD von Master Branch direkt zu Umgebung, mit der User interagiert
  - Download von GitHub (`devtools::install_github()`)
--

- Kollaboration: unabhängige Entwicklung von Code ("feature branches")
--

- Experimente: Branch einfach wieder löschen. 
--

- example: [https://github.com/jandix/sealr/branches](https://github.com/jandix/sealr/branches)

---

## Branches Workflow

1. Branch in Gitkraken erstellen mit aussagekräftigem Namen (z.B. `issue1-add-favorite-gif`)  
--

2. Branch auschecken (Doppelklick on Icon in Gitkraken)
--

3. normal weiterarbeiten (pull-commit-push cycles)
--

4. (optional: merge andere branches in deinen branch um Updates zu bekommen)
--

5. merge Branch in master Branch 



---

## Branches mergen

- Rechtsclick auf branch name / master
- hängt davon ab, wer "weiter vorne" ist (?)
    - wenn neue Commits auf master: merge master into `issue1-add-favorite-gif` -> branch wird geupdatet
    - wenn neue Commits auf branch: merge `issue1-add-favorite-gif` into master -> master wird geupdatet
    
---

## Pull Requests (PRs)

> Pull requests let you tell others about changes you've pushed to a branch in a repository on GitHub. Once a pull request is opened, you can discuss and review the potential changes with collaborators and add follow-up commits before your changes are merged into the base branch. ([Source](https://help.github.com/en/articles/about-pull-requests))


- zusammen mit Branches verwenden
- nützlich für Review + Feedback (extra Tooling in GitHub)

---

## Hands on 8 - branches & pull requests

1. Grace & Mona together: erstelle einen neuen Issue (seid kreativ!)
2. Mona: erstelle lokal einen Branch mit dem Namen `issue2-{beschreibung}` 
3. Mona & Grace: arbeitet zusammen auf Mona's Laptop an Issue 2
4. Mona: committen + Branch pushen
5. Mona: öffne einen Pull Request und weise Grace als Reviewer zu
6. Grace: review Mona's Pull Request und merge ihn in den Master Branch

---

## Real life: Branches und Pull Requests 
- Branches
  - sehr nützlich für Entwicklung von R Packages / Applikationen / Shiny Apps in einem Team --> Feature Branches
  - Relevanz für Datenanalyse-Projekte: parallel an verschiedenen Ansätzen arbeiten, inkrementiell an unfertigem Code arbeiten (Master frei halten von unfertigem Code), Backup (!)
- Pull Requests
  - zentralisierte Diskussion
  - code review Stage


---
class: center, middle, inverse

# Recap

---

## Heute..

- ein GitHub Repo forken und clonen
--

- einen Commit in Gitkraken machen
--

- Commits von GitHub pullen und zu GitHub pushen
--

- mit Branches und Pull Requests arbeiten

---
class: middle, center, inverse

# That's it! 

---

## Wenn du Hilfe brauchst...

### Frie Preu
 + `r icon::fa("briefcase")` data scientist / IT consultant @ [codecentric AG](https://www.codecentric.de)
 + `r icon::fa("github")` [friep](https://github.com/friep)
 + `r icon::fa("github")` [friep](https://gitlab.com/friep)
 + `r icon::fa("keybase")` [friep](https://keybase.io/friep)
 + `r icon::fa("twitter")` [ameisen_strasse](https://twitter.com/ameisen_strasse)

