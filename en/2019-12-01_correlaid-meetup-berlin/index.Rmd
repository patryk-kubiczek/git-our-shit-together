---
title: "Let's git our sh*t together!"
author: "Frie Preu"
date: "2019-12-01"
output:
  xaringan::moon_reader:
    css: ["default", "metropolis", "metropolis-fonts", "custom.css"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

## Who am I? 
- Frie Preu
- data scientist / IT consultant / software developer at codecentric 
- volunteer at CorrelAid: IT infrastructure, internal projects (R packages), miscellaneous mischief 
- Add me:
    - LinkedIn: [Friedrike Preu](https://www.linkedin.com/in/friedrike-preu-a2bb46a7/)
    - Twitter: [@ameisen_strasse](https://twitter.com/ameisen_strasse)
    - GitHub: [friep](https://github.com/friep)
    - GitLab: [friep](https://gitlab.com/friep)

---


## Why Git?

- masterthesis.docx
--

- masterthesis_v1.docx
--

- masterthesis_FINAL.docx
--

- masterthesis_FINAL_TimsEdits.docx
--

- masterthesis_FINAL_FINAL.docx

---

class: center, middle 

![Help](https://media.giphy.com/media/phJ6eMRFYI6CQ/giphy.gif)

---

## Version Control to the Rescue!
- Example: [https://gitlab.com/friep/git-our-shit-together-test-repo/](https://gitlab.com/friep/git-our-shit-together-test-repo/)
- just a simple README.md for you 

---

## Partner up!

.pull-left[
  ### Ada Lovalace 
  <img src="../../images/other/ada_lovelace.jpg" height="350">


-> [Ada Lovelace](https://en.wikipedia.org/wiki/Ada_Lovelace)

]
.pull-right[
  ### Katherine Johnson
  <img src="../../images/other/katherine_johnson.jpg" height="350">

-> [Katherine Johnson](https://en.wikipedia.org/wiki/Katherine_Johnson)

]

---

## Install requirements for hands on!

- see "Requirements for Hands-On Sessions" in the `README` of the repository: [https://gitlab.com/friep/git-our-shit-together](www.gitlab.com/friep/git-our-shit-together)


---
## VSCode
open-source editor by Microsoft with many extensions
.pull-left[
  .right[

    ![VSCode Sidebar](../../images/vscode/vscode_sidebar.png)
  ]
]
.pull-right[
  .large[
  
- file explorer
  
- git tools
  
- search through folder

- (debug)
  
- **marketplace**

  ]
  
  
]

---

## Install VSCode extensions
- search and install:
  - GitLens
  - Git Graph


`r emo::ji("tada")` 

---

## GitLab and Git
- see "Requirements for Hands-On Sessions" in the `README` of the repository: [https://gitlab.com/friep/git-our-shit-together](www.gitlab.com/friep/git-our-shit-together)
  - create GitLab account
  - install Git

`r emo::ji("tada")` `r emo::ji("tada")` 


---

class: center, middle, inverse

# Download data - fork and clone

---

## Fork and clone

### Repository
> A Git repository is a virtual storage of your project. It allows you to save versions of your code, which you can access when needed. ([Source](https://www.atlassian.com/git/tutorials/setting-up-a-repository))


---

## Fork and Clone

### Fork
> A fork is a copy of a repository. Forking a repository allows you to freely experiment with changes without affecting the original project. ([Source](https://help.github.com/articles/fork-a-repo/))

### Clone 
> Cloning a repository syncs it to your local machine. After you clone, you can add and edit files and then push and pull updates.  ([Source](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html))

---

## Hands On 1 - fork a repo
 
### Ada


1.  [https://gitlab.com/friep/git-our-shit-together-test-repo/](https://gitlab.com/friep/git-our-shit-together-test-repo/): Fork (top right)
2. `https://gitlab.com/{USERNAME}/git-our-shit-together-test-repo` opens
3. go to Settings->Members and invite Katherine as a maintainer


---

## Still nothing on my laptop!!!

<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/26n6xBpxNXExDfuKc" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/GeneralHospital-time-gh-26n6xBpxNXExDfuKc">via GIPHY</a></p>

---
class: center, middle, inverse

# Detour: Git authentication  - SSH vs HTTPS 
---

## Authentification 

**GitLab needs to know that you are you!**

.pull-left[
  ### HTTPS
  - with GitLab username + password -> easy to understand
  - possibly need to enter username + password over and over
  - clone `https://...` 
]
.pull-right[
 ### SSH
 - with a *keypair* (`id_rsa.pub` and `id_rsa`)
 - public key, private key cryptography (see e.g. [Youtube](https://www.youtube.com/watch?v=AQDCe585Lnc))
 - advantage: only set up once, no need to remember password
 - clone `ssh://...`
]

---
## Hands On 2: create SSH keypair

1. Check for pre-existing keys
- windows: open Git Bash, enter `ls -al ~/.ssh` 
- Linux / Mac: open terminal, enter `ls ~/.ssh`

2. create key if none exists:
- enter `ssh-keygen -t rsa -C "your_email@example.com"`  (replace with email you used for GitLab registration!)
- accept defaults (default location, no password)

---

## Hands On 3: add public key to GitLab

3. copy to GitLab 
- copy the public key from `id_rsa.pub`
  - Windows: in Git Bash, enter `clip < ~/.ssh/id_rsa.pub`
  - Linux / Mac: `cat ~/.ssh/id_rsa.pub` -> copy the output 
- add to GitLab
  - go to the [SSH keys](https://gitlab.com/profile/keys) section of the GitLab settings and copy your key into the box. Give a meaningful name and click "add key"


---

<div style="width:100%;height:0;padding-bottom:93%;position:relative;"><iframe src="https://giphy.com/embed/13mbTHVskEHyGA" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/yes-applause-shakira-13mbTHVskEHyGA">via GIPHY</a></p>


---

## Hands On 4 - clone a repo
### Ada & Katherine

.pull-left[

- VSCode: `CTRL + SHIFT + P`: "Git: Clone"
  - remember to use the *correct* URL (SSH or HTTPS?? `r emo::ji("eyes")`)
- select repository location
]

.pull-right[

![Git clone](../../images/gitlab/gitclone.png) 
]

---


## Real life 
- authentication
  - SSH keys
  - GitLab tokens as password 
  - ...
- open source contribution: fork + clone
- job / collab with friends: only clone

---

class: center, middle, inverse


# Save your files - Add und Commit 

---
## Commit

### Commit 
> A commit is the Git equivalent of a "save".[...] Git committing is an operation that acts upon a collection of files and directories. ([Source](https://www.atlassian.com/git/tutorials/saving-changes))

--> Commit = a "save point" in Git. 

---

## Commit

- a commit saves **changes** w.r.t. the previous commit
  - edits of file(s)
  - creation of new file(s)
  - deletion of file(s)
  - renaming of file(s)
- a commit can contain several changes!

---

## Git history with Git Graph
![vscode sidebar](../../images/vscode/vscode_gitgraph_button.png)
- OR: CTRL + SHIFT + P: "Git Graph: View Git Graph"

---

## Git history with Git Graph
![git history](../../images/vscode/vscode_git_history.png)

---

class: center

## Go back in time! 

![Time Machine](https://media.giphy.com/media/Vqvr9BGv1vhDi/giphy.gif)

---

## Hands On 5 - Go back in time 

### Ada and Katherine

1. right-click on a commit: `reset current branch to this commit`
2. play with: `hard`, `mixed`, `soft`
3. commit at top: `reset current branch to this commit -> hard` to finish exercise 

---


class: center

## Making a commit - Adding and Committing 

![Staging Area](../../images/other/staging.png)

(Source: [https://git-scm.com/about/staging-area](https://git-scm.com/about/staging-area))

---

## Making a commit - Adding and Committing

![Commit panel VSCode](../../images/vscode/vscode_commit_panel.png)

---

## Making a commit - Adding and Committing 

.pull-left[

  
- Staged files / staging area: 

![Commit panel VSCode](../../images/vscode/vscode_commit_panel_staging_area.jpg)


  
- Types of changes (A: added, M: modified, D: deleted, R: renamed, ...)
  
  ![Commit panel VSCode](../../images/vscode/vscode_commit_panel_change_type.jpg)
]

.pull-right[

- Unstaged files

![Commit panel VSCode](../../images/vscode/vscode_commit_panel_unstaged.jpg)

- View diff, discard (delete) changes, add to staging area 

  ![Commit panel VSCode](../../images/vscode/vscode_commit_panel_file_controls.jpg)

- Commit message and commit button: 

 ![Commit panel VSCode](../../images/vscode/vscode_commit_panel_commit_message.jpg)

]




---

class: center, middle, inverse

# Detour: Markdown

---

## Markdown
- a markup language
- easy way to write...
  - notes 
  - texts
  - presentations
  - ...
- and *render* them to good looking documents!
- with `README.md` open: CTRL + SHIFT + P: "Markdown: Open Preview" 

---

## Markdown syntax
- `#(###)`: make a header
- `*` or `-`: make bullet point lists
- `[click here](https://git-scm.com/docs/)`: add a link
- ... many more, see [https://www.markdownguide.org/basic-syntax/](https://www.markdownguide.org/basic-syntax/)

---

## Hands On 6 - make a commit

### Katherine + Ada

1.  Change the `README.md`, add an image to `images`, etc.
2. **GIT ADD** the "unstaged files" you want to commit / "save in Git"  
3. write a (at least partly) informative "commit message"
4. **GIT COMMIT** 


---
class: center, middle


## Git quizzed!

![Lokal](../../images/other/git_workflow_lokal_without_solution.png)

---
class: center, middle

## Git quizzed!


![Lokal](../../images/other/git_workflow_lokal_with_solution.png)

---
class: center, middle, inverse

# Sync files - Push and Pull

---


## Git  Hosting

- GitHub: `r icon::fa("github")` 
- GitLab: `r icon::fa("gitlab")` 
- Keybase: `r icon::fa("keybase")` 

---

## Git local and git remote

... what? 

- **local**: your laptop / machine
- **remote**: in the Cloud (GitLab, GitHub...)

![Gitkraken Lokal Remote](../../images/vscode/vscode_remote.png)

---


## Sync: Git Pull and Git Push

- Git Pull: "download" new commits from GitLab
- Git Push: "upload" the commits you made locally to GitLab
---
class: center, middle

## Sync: Git Pull and Git Push

![Push Pull](../../images/vscode/vscode_push_pull.jpg)

or:
- CTRL + SHIFT + P: Git Pull / Push

---
class: center, middle

![Push the button](https://media.giphy.com/media/139lMwJ9ow7bKE/giphy.gif)

---


## Hands On 7 - Pull and Push

1. Katherine: Push 
2. Ada: Pull 
3. Ada: Push 
4. Katherine: Pull

---
class: center, middle

## Git quizzed!

![Push Pull](../../images/other/git_workflow_with_github_without_solution.png)

---


## Git quizzed!

![Push Pull](../../images/other/git_workflow_with_github_with_solution.png)

---
class: center, middle, inverse

# When things go wrong...

---
class: center, middle

![git google](../../images/other/giterrors.png)

---
class: center, middle

![xccd](../../images/other/xkcd_comic.png)


---

## When things go wrong...

1. as long nothing is pushed, all is (kind of) ok -> don't push if things are messed up!
2. if things are really messed up: save your code somewhere else and clone again

---

## Merge conflicts

> Merge conflicts occur when competing changes are made to the same line of a file, or when one person edits a file and another person deletes the same file. ([Source](https://help.github.com/en/articles/resolving-a-merge-conflict-using-the-command-line))

---

## Merge conflicts
1. you commit changes 
--

2. you pull to get the newest updates
--

3. your coworker has changed something that clashes with your changes
--

4. git: 

![mc warn](../../images/vscode/vscode_warn_mc.png)

---
class: middle, center

![up to you](https://media.giphy.com/media/xUOxfcCFf8z89a6KTC/giphy.gif)

---
## Solving merge conflicts

- **you** need to solve merge conflicts on your machine -> decide which version / changes you keep
- VSCode has an integrated [merge conflict tool](https://www.youtube.com/watch?v=kBIMGOxqqnk)


---
## Solving merge conflicts
- *current change*: **your** change
- *incoming change*: from GitLab / other branch /...
![mc tool](../../images/vscode/vscode_mc_tool.png)

---

## Hands On 8: Merge conflicts

1. Ada or Katherine: edit `README.md` **on GitLab** in a certain line
2. Ada & Katherine: edit `README.md` locally on your machine **in the same line** (edit it differently!)
3. Adat & Katherine: commit your own changes (don't push yet!)
4. Ada & Katherine: `pull` 
5. Ada & Katherine: solve the merge conflict locally and push your own version.



---
class: center, middle, inverse

# work with GitLab

---
class: center, middle, inverse

# Issues

---

## Issues


- issues: todos / bugs / ideas
- each issue has a number 
- \#issueno in commit message associates commit with issue

- example: [https://gitlab.com/gitlab-org/gitlab/issues](https://gitlab.com/gitlab-org/gitlab/issues) 

---

## Hands On 9: Issues


1. Ada: create an issue "add Katherine's favorite GIF" 
2. Katherine: go to [giphy.com](giphy.com), find a funny gif and add it to the presentation  (-> copy link)
3. Katherine: add + commit. link the issue number in the commit message (#issueno)
4. Katherine: push
5. Ada: reload the issue (ctrl + r) 

---

## Recap issues

- very useful for project organization 
- discussion is centralized (**but**: don't put sensitive information in GitLab)
- Tip: create a GitLab *board* for process tracking


---
class: center, inverse, middle 

# Branches and Merge Requests

---
class: center, middle


![Gitkraken branching](../../images/vscode/vscode_branches.png)

---
class: center, middle

![Come on](https://media.giphy.com/media/HfFccPJv7a9k4/giphy.gif)

---


## Branches

### Branch
> A branch represents an independent line of development. Branches serve as an abstraction for the edit/stage/commit process. You can think of them as a way to request a brand new working directory, staging area, and project history. ([Source](https://www.atlassian.com/git/tutorials/using-branches))


### Checkout

> The git checkout command lets you navigate between the branches created by git branch. Checking out a branch updates the files in the working directory to match the version stored in that branch, and it tells Git to record all new commits on that branch. Think of it as a way to select which line of development you’re working on. ([Source](https://www.atlassian.com/git/tutorials/using-branches/git-checkout))

---

## Why branches?

- stability: only have working code on "master" branch 
  - e.g. CI/CD directly to dev
  - users download your code from GitLab / GitHub (e.g. `devtools::install_github()`)
--

- collaboration: independent development of code ("feature branches") 
--

- experiments: try it out in a branch
--

- example: [https://github.com/jandix/sealr/branches](https://github.com/jandix/sealr/branches)

---

## Branches Workflow

1. create a branch in Git with a meaningful name (e.g. issue1-add-favorite-gif): CTRL + SHIFT + P "Create branch"
--

2. checkout branch: branch is automatically checked out (blue circle next to name)
--

3. continue working as usual (pull-commit-push cycles)
--

4. (optional: merge other branches into your branch to get their updates: right-click on branch, "Merge into current branch")
--

5. merge your branch into the master branch (checkout master, then see 4.)



---


---

## Merge Requests (MRs)

> Merge requests allow you to visualize and collaborate on the proposed changes to source code that exist as commits on a given Git branch. A Merge Request (MR) is the basis of GitLab as a code collaboration and version control platform. It is as simple as the name implies: a request to merge one branch into another.
 ([Source](https://docs.gitlab.com/ee/user/project/merge_requests/))


- useful "review" and feedback functionality 
- GitHub: "Pull Request"

---

## Hands On 10 - branches and merge requests

1. Katherine & Ada together: create a new issue (think of something!)
2. Ada: create a branch called `issue2-{description}` locally
3. Ada & Katherine: work together on issue 2 on Ada's machine
4. Ada: push the branch 
5. Ada: open a merge request and assign Katherine as a reviewer
6. Katherine: review Ada's MR and merge it

---

## Real life: Branches and merge Requests 
- branches
  - very useful when developing R packages / applications / shiny apps in a team: feature branches
  - relevance for data analysis projects: enables working in parallel on several methods, keep master clean of non-working code, backup (!)
- merge requests
  - centralize discussion
  - code review stage


---

class: center, middle, inverse
# Miscellaneous

---

## Git stash

> git stash temporarily shelves (or stashes) changes you've made to your working copy so you can work on something else, and then come back and re-apply them later on. Stashing is handy if you need to quickly switch context and work on something else, but you're mid-way through a code change and aren't quite ready to commit. ([Source](https://www.atlassian.com/git/tutorials/saving-changes/git-stash))

---
class: center

## Git stash

![stash](https://media.giphy.com/media/l0HlxJfVUKhtR8Jna/giphy.gif)

- put it away for now!
- useful for (temporarily) storing stuff not ready to commit 

--- 

# Recap

---

## What we learned today

- Fork and clone a GitLab repository
--

- make a commit in VScode
--

- pull and push commits from / to GitLab 
--

- work with branches and merge requests in VSCode

---
class: middle, center, inverse

# That's it! 

---

## If you ever need help...contact me!

### Frie Preu
 + `r icon::fa("briefcase")` data scientist / IT consultant @ [codecentric AG](https://www.codecentric.de)
 + `r icon::fa("github")` [friep](https://github.com/friep)
 + `r icon::fa("gitlab")` [friep](https://gitlab.com/friep)
 + `r icon::fa("keybase")` [friep](https://keybase.io/friep)
 + `r icon::fa("twitter")` [ameisen_strasse](https://twitter.com/ameisen_strasse)
 + `r icon::fa("slack")` [RLadies Slack](https://rladies-community-slack.herokuapp.com/)
